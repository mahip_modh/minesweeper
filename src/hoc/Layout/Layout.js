import React,{ Component } from "react";
import Aux from "../Aux/Aux";
import MineSweeper from "../../container/MineSweeper/MineSweeper";

class Layout extends Component{

    render(){
        return(
            <Aux>
                <MineSweeper />
            </Aux>
        )
    }
}

export default Layout;