import React, { Component } from 'react';
import MineGrid from "../../component/MineGrid/MineGrid"

class MineSweeper extends Component {

    state = {
        grid: [[0, 0], [0, 0]],
        outputGrid:[],
        gridSize: 2,
        decreaseGrid: false,
        increaseGrid: true
    }

    gridIncreaseHandler = () => {
        const oldSize = this.state.gridSize;
        const updatedSize = oldSize + 1;
        let inputArr = new Array(updatedSize);
        for (let i = 0; i < updatedSize; i++) {
            inputArr[i] = new Array(updatedSize);
        }
        for (let i = 0; i < inputArr.length; i++) {
            let width = inputArr[i];
            for (let j = 0; j < width.length; j++) {
                inputArr[i][j] = 0
            }
        }
        this.setState({ gridSize: updatedSize, decreaseGrid: true, grid: inputArr });
        if (updatedSize === 9)
            this.setState({ gridSize: updatedSize, increaseGrid: false });
    }

    gridDecreaseHandler = () => {
        const oldSize = this.state.gridSize;
        const updatedSize = oldSize - 1;
        var inputArr = new Array(updatedSize);
        for (let i = 0; i < updatedSize; i++) {
            inputArr[i] = new Array(updatedSize);
        }
        for (let i = 0; i < inputArr.length; i++) {
            let width = inputArr[i];
            for (let j = 0; j < width.length; j++) {
                inputArr[i][j] = 0
            }
        }
        this.setState({ gridSize: updatedSize, grid: inputArr, increaseGrid: true });
        if (updatedSize === 0)
            this.setState({ gridSize: updatedSize, decreaseGrid: false });
    }

    inputChangeHandler = (index, index1) => {
        const arr = [...this.state.grid];
        if (arr[index][index1] === 0) {
            arr[index][index1] = 1;
        } else {
            arr[index][index1] = 0;
        }
        this.setState({ grid: arr });
    }

    calculateMinesHandler = () => {
        const arr = [...this.state.grid];
        const size = this.state.gridSize;
        let outputArr = new Array(size);
        let mines;
        for (let i = 0; i < size; i++) {
            outputArr[i] = new Array(size);
        }
        for (let i = 0; i < outputArr.length; i++) {
            let width = outputArr[i];
            for (let j = 0; j < width.length; j++) {
                mines =this.calculateSum(arr,i,j,size);
                outputArr[i][j] = mines;
            }
        }
                this.setState({outputGrid:outputArr})
    }

     calculateSum = (arr, row, column, size) =>{
        let sum = 0;
        for (let left = row - 1; left <= row + 1; left++) {
            for (let down = column - 1; down <= column + 1; down++) {
                if (left >= 0 && down >= 0 && left < size && down < size && !(left === row && down === column)) 
                { sum = sum + arr[left][down]; }
                if(arr[row][column]===1)
                {sum = 9;}
            }
        }
        return sum;
    }

    render() {
        return (
            <MineGrid
                input={this.state.grid}
                output={this.state.outputGrid}
                size={this.state.gridSize}
                gridIncrease={() => { this.gridIncreaseHandler() }}
                gridDecrease={this.gridDecreaseHandler}
                decreasable={this.state.decreaseGrid}
                incresable={this.state.increaseGrid}
                inputChange={this.inputChangeHandler}
                calculateMines={this.calculateMinesHandler}
            />
        );
    }
}

export default MineSweeper;