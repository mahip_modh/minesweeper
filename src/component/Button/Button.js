import React from "react";
import "./Button.css";

const button = (props) => (
    <button
        // className={[classes.Button, classes[props.btnType]].join(' ')}
        className={['Button', props.btnType].join(' ')}
        onClick={props.clicked}
        disabled={props.disabled}>
        {props.children}
    </button>
);

export default button;