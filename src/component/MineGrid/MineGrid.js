import React from 'react';
import Button from "../Button/Button";
import Aux from "../../hoc/Aux/Aux";
import "./MineGrid.css";

const mineGrid = (props) => {
    return (
        <Aux>
            <div>
                <p>Select grid size : <strong>{props.size}</strong> * <strong>{props.size}</strong>
                    <Button btnType="Danger"
                        disabled={!props.decreasable}
                        clicked={props.gridDecrease} > - </Button>
                    <Button btnType="Success" 
                    clicked={props.gridIncrease} 
                    disabled={!props.incresable}> + </Button>
                </p>
            </div>
            <div>
                <table>
                    <tbody>
                    {props.input.map((listValue, index) => {
                        return (
                            <tr key={index} >
                                {listValue.map((dataValue, index1) => {
                                    return (
                                        <td key={index1} className={'Td'} onClick={() => props.inputChange(index, index1)}>{dataValue}</td>
                                    );
                                })}
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
                </div>
            <div>
                <Button btnType="Submit"
                        clicked={props.calculateMines} > Submit </Button>
            </div>
            <div >
            <table>
                    <tbody>
                    {props.output.map((listValue, index) => {
                        return (
                            <tr key={index} >
                                {listValue.map((dataValue, index1) => {
                                    return (
                                        <td key={index1} className={'Td'} onClick={() => props.inputChange(index, index1)}>{dataValue}</td>
                                    );
                                })}
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
            
        </Aux>
    );
}

export default mineGrid;